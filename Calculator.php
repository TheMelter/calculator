<?php
/**
 * @author Hyunmin Kim
 */
class Calculator
{
    const REGEX_OPERATIONS = "/[\%\-\*\+\/]/";
    const REGEX_NUMBERS = "/\d+/";
    const ORDER_MULT_DIV_MOD = 1;
    const ORDER_SUB_ADD = 2;

    private $operations;
    private $numbers;

    /**
     * Process statement and return result.
     */
    public function run($statement)
    {
        preg_match_all(self::REGEX_OPERATIONS, $statement, $this->operations);
        preg_match_all(self::REGEX_NUMBERS, $statement, $this->numbers);
        $this->operations = $this->operations[0];
        $this->numbers = $this->numbers[0];

        $currentOrder = self::ORDER_MULT_DIV_MOD;

        $result = null;

        // Run while statement still has operations left.
        while (count($this->operations) > 0) {
            foreach ($this->operations as $index => $operation) {
                if ($this->getOperationOrder($operation) == $currentOrder) {
                    $result = $this->operate($this->numbers[$index], $this->numbers[$index + 1], $operation);
                    $this->sortOperationsAndNumbers($result, $index);

                    break;
                } 

                if ($index == count($this->operations) - 1) {
                    $currentOrder++;
                }
            }
        }

        return $result;
    }

    /**
     * Replaces used operation along with corresponding numbers with new result.
     */
    private function sortOperationsAndNumbers($result, $index)
    {
        unset($this->numbers[$index]);
        unset($this->numbers[$index + 1]);
        unset($this->operations[$index]);

        array_splice($this->numbers, $index, 0, array($result));
        $this->operations = array_values($this->operations);
    }

    /**
     * Operate substatement.
     */
    private function operate($num1, $num2, $operation)
    {
        $num1 = (int)$num1;
        $num2 = (int)$num2;

        if ($operation == "+") {
            return $this->add($num1, $num2);
        } elseif ($operation == "-") {
            return $this->subtract($num1, $num2);
        } elseif ($operation == "*") {
            return $this->multiply($num1, $num2);
        } elseif ($operation == "/") {
            return $this->divide($num1, $num2);
        } elseif ($operation == "%") {
            return $this->modulus($num1, $num2);
        }
    }

    /**
     * Returns of order of operations.
     */
    private function getOperationOrder($operation)
    {
        if ($operation == "+") {
            return self::ORDER_SUB_ADD;
        } elseif ($operation == "-") {
            return self::ORDER_SUB_ADD;
        } elseif ($operation == "*") {
            return self::ORDER_MULT_DIV_MOD;
        } elseif ($operation == "/") {
            return self::ORDER_MULT_DIV_MOD;
        } elseif ($operation == "%") {
            return self::ORDER_MULT_DIV_MOD;
        }
    }

    private function add($num1, $num2)
    {
        return $num1 + $num2;
    }

    private function subtract($num1, $num2)
    {
        return $num1 - $num2;
    }

    private function multiply($num1, $num2)
    {
        return $num1 * $num2;
    }

    private function divide($num1, $num2)
    {
        return $num1 / $num2;
    }

    private function modulus($num1, $num2)
    {
        return $num1 % $num2;
    }

    /**
     * Returns array of operations.
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Returns array of numbers.
     */
    public function getNumbers()
    {
        return $this->numbers;
    }
}
