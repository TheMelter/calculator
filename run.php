<?php
require_once "Calculator.php";

$calc = new Calculator();
$result1 = $calc->run("1 + 2 + 3 * 4 / 4");
$result2 = $calc->run("2 / 2 * 3 + 4 / 1");
$result3 = $calc->run("2 / 2 * 3 + 4 % 3");
$result4 = $calc->run("1 * 2 + 2 % 2");

echo "$result1\n";
echo "$result2\n";
echo "$result3\n";
echo "$result4\n";
